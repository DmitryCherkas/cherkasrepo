package com.by.cherkas.fileservice.controller;

import com.by.cherkas.fileservice.LoggerWrapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@RestController
public class FileController {

    private static final LoggerWrapper LOG = LoggerWrapper.get(FileController.class);

    @Value("${upload.path}")
    private String uploadPath;

    @PostMapping("/image")
    public ResponseEntity<String> getImageUri(@RequestParam("file") MultipartFile file) {

        String uri = "";

        if (file != null && !Objects.requireNonNull(file.getOriginalFilename()).isEmpty()) {
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }

            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            try {
                file.transferTo(new File(uploadPath + "/" + resultFilename));
            } catch (IOException e) {
                LOG.error(String.format("exception in writing tempFile", e.getMessage()));
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            uri = "file:///" + uploadPath + "/" + resultFilename;
        }

        return ResponseEntity.ok(uri);
    }
}
