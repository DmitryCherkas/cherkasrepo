package com.by.cherkas.gateway.conroller;

import com.by.cherkas.gateway.LoggerWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
public class GatewayController {

    private static final LoggerWrapper LOG = LoggerWrapper.get(GatewayController.class);

    @Autowired
    private RestTemplate restTemplate;

    @Order
    @PostMapping("/addUser/{userName}/{userEmail}/{userStatus}")
    public ResponseEntity<Integer> addNewUser(@PathVariable("userName")String userName, @PathVariable("userEmail")String userEmail,
                                              @PathVariable("userStatus")String userStatus, @RequestParam("file") MultipartFile file){

        URI uri = null;
        try {
            uri = new URI("http://user-service/user/" + userName + "/" + userEmail + "/" + userStatus);
        } catch (URISyntaxException e) {
            LOG.error("exception in creating user", "wrong URI", 0);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String dir = "D:/ProjectsForPortfolio/MicroserviceTask/gateway/";

        File tempFile = null;
        String tmpImageFileName = dir + file.getOriginalFilename();
        try {
            tempFile = new File(tmpImageFileName);
            file.transferTo(tempFile);
        } catch (IOException e) {
            LOG.error(String.format("exception in writing tempFile", e.getMessage()));
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(tmpImageFileName));

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(body, headers);
        ResponseEntity<Integer> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, Integer.class);

        if (tempFile.exists()) { tempFile.delete(); }

        assert responseEntity != null;
        return ResponseEntity.ok(responseEntity.getBody());
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @GetMapping("/getUser/{userId}")
    public ResponseEntity<Map> getUser(@PathVariable("userId")Integer userId){

        URI uri = null;
        try {
            uri = new URI("http://user-service/user/" + userId);
        } catch (URISyntaxException e) {
            LOG.error("exception in getting user", "wrong URI", 0);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<HashMap> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, HashMap.class);

        return ResponseEntity.ok(responseEntity.getBody());
    }

    @Order
    @GetMapping("/changeStatus/{userId}/{newStatus}")
    public ResponseEntity<Map> changeStatus(@PathVariable("userId")Integer userId,
                                            @PathVariable("newStatus") String newStatus){

        URI uri = null;
        try {
            uri = new URI("http://user-service/user/" + userId + "/" + newStatus);
        } catch (URISyntaxException e) {
            LOG.error("exception in changing user status", "wrong URI", 0);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<HashMap> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, HashMap.class);

        return ResponseEntity.ok(responseEntity.getBody());
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @GetMapping("/server/{status}")
    public ResponseEntity serverStatus(@PathVariable("status") String status) {

        URI uri = null;
        try {
            uri = new URI("http://user-service/user/status/" + status);
        } catch (URISyntaxException e) {
            LOG.error("exception in getting server status", "wrong URI", 0);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<ArrayList> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, ArrayList.class);

        return ResponseEntity.ok(responseEntity.getBody());

    }
}
