package com.by.cherkas.userservice.controller;


import com.by.cherkas.userservice.LoggerWrapper;
import com.by.cherkas.userservice.entity.Users;
import com.by.cherkas.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    private static final LoggerWrapper LOG = LoggerWrapper.get(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping("/user/{userName}/{userEmail}/{userStatus}")
    public ResponseEntity<Integer> createUser(@PathVariable("userName")String userName, @PathVariable("userEmail")String userEmail,
                                              @PathVariable("userStatus")String userStatus, @RequestParam("file") MultipartFile file){
        Users user = new Users();

        URI uri = null;
        try {
            uri = new URI("http://file-service/image");
        } catch (URISyntaxException e) {
            LOG.error("exception in creating user", "wrong URI", 0);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String dir = "D:/ProjectsForPortfolio/MicroserviceTask/gateway/";

        File tempFile = null;
        String tmpImageFileName = dir + file.getOriginalFilename();
        try {
            tempFile = new File(tmpImageFileName);
            file.transferTo(tempFile);
        } catch (IOException e) {
            LOG.error(String.format("exception in writing tempFile", e.getMessage()));
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(tmpImageFileName));

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, String.class);
        user.setUserName(userName);
        user.setUserEmail(userEmail);
        user.setImageUri(responseEntity.getBody());
        user.setUserStatus(userStatus);

        userService.createUser(user);

        return new ResponseEntity<>(user.getUserId(), HttpStatus.CREATED);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<Map<String, String>> getUserById(@PathVariable("userId")Integer userId) {

        if(userId == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Users user = userService.getUserById(userId);

        if(user == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Map<String, String> userData = new HashMap<>();
        userData.put("userName", user.getUserName());
        userData.put("userEmail", user.getUserEmail());
        userData.put("userStatus", user.getUserStatus());
        userData.put("imageUri", user.getImageUri());

        return ResponseEntity.ok(userData);
    }

    @GetMapping("/user/{userId}/{newStatus}")
    public ResponseEntity<Map<String, String>> changeUsersStatus(@PathVariable("userId")Integer userId,
                                                                 @PathVariable("newStatus") String newStatus) {

        if(userId == null || newStatus == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Map<String, String> userInfo = userService.changeUsersStatus(userId, newStatus);

        if(userInfo == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else return ResponseEntity.ok(userInfo);
    }

    @GetMapping("/user/status/{status}")
    public ResponseEntity<List<Map<String, String>>> changeUsersStatus(@PathVariable("status") String status){

        if(status == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        List<Map<String, String>> userInfo = userService.getServerStatus(status);

        if(userInfo == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else return ResponseEntity.ok(userInfo);
    }
}
