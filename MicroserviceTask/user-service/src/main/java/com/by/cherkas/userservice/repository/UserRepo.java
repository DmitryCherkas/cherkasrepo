package com.by.cherkas.userservice.repository;

import com.by.cherkas.userservice.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;

public interface UserRepo extends JpaRepository<Users, Integer> {
}
