package com.by.cherkas.userservice.service;

import com.by.cherkas.userservice.entity.Users;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * Create User
     *
     * @param user {@link Users}
     */
    int createUser(Users user);
    /**
     * Get User
     *
     * @param userId {@link Users} id
     * @return existing {@link Users} entry or <i>null</i> if {@link Users} with specified id was not found
     */
    Users getUserById(int userId);
    /**
     * Change User's Status
     *
     * @return {@link Map<String, String>}
     */
    Map<String, String> changeUsersStatus(int userId, String newStatus);
    /**
     * Get Server Status
     *
     * @return {@link List<Map<String, String>>}
     */
    List<Map<String, String>> getServerStatus(String status);
}
