package com.by.cherkas.userservice.service;

import com.by.cherkas.userservice.entity.Users;
import com.by.cherkas.userservice.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public int createUser(Users user) {
        userRepo.save(user);
        return user.getUserId();
    }

    @Override
    public Users getUserById(int userId) {
        return userRepo.getOne(userId);
    }

    @Override
    public Map<String, String> changeUsersStatus(int userId, String newStatus) {

        Map<String, String> response = new HashMap<>();
        Users user = userRepo.getOne(userId);
        String oldStatus = user.getUserStatus();

        if(newStatus.equals("ONLINE") || newStatus.equals("OFFLINE")){
            user.setUserStatus(newStatus);
            userRepo.save(user);
            response.put("userId", String.valueOf(userId));
            response.put("newStatus", user.getUserStatus());
            response.put("oldStatus", oldStatus);
            return response;
        }
        return null;
    }

    @Override
    public List<Map<String, String>> getServerStatus(String status) {

        List<Map<String, String>> sortList = new ArrayList<>();
        List<Users> usersList = userRepo.findAll();
        int count = 0;

        for (Users user : usersList) {
            Map<String, String> usersMap = new HashMap<>();
            if(user.getUserStatus().equals(status)){
                count++;
                usersMap.put("userId" + count, String.valueOf(user.getUserId()));
                usersMap.put("userName" + count, user.getUserName());
                usersMap.put("userStatus" + count, user.getUserStatus());
                usersMap.put("imageUri" + count, user.getImageUri());
                sortList.add(usersMap);
            }
        }
        return sortList;
    }


}