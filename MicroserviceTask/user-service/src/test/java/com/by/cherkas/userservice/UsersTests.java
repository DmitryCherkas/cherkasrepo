package com.by.cherkas.userservice;

import com.by.cherkas.userservice.entity.Users;
import com.by.cherkas.userservice.service.UserService;
import com.by.cherkas.userservice.service.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UsersTests {

    private Users mockUser = new Users("image/batman.jpg", "Bruse", "wane@mail.ru", "OFFLINE");

    @Mock
    private UserService userServiceMock;

    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @Test
    public void testOnGettingUserById() {
        Mockito.when(userServiceMock.getUserById(Mockito.anyInt())).thenReturn(mockUser);
        assertEquals(mockUser, userServiceMock.getUserById(mockUser.getUserId()));
    }

    @Test
    public void testOnCreatingUser() {
        userServiceMock.createUser(mockUser);
        Mockito.when(userServiceMock.getUserById(Mockito.anyInt())).thenReturn(mockUser);
        assertEquals(mockUser, userServiceMock.getUserById(mockUser.getUserId()));
    }

}
