<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link href="<c:url value="/css/style.css"/>" rel="stylesheet">
    <title>MainPage</title>
</head>
<body>
<div class="container">
    <h1>List of Users</h1>
    <table>
        <tr>
            <th>LOGIN</th>
            <th>FIRST NAME</th>
            <th>LAST NAME</th>
        </tr>
        <c:forEach items="${userList}" var="user">
            <tr>
                <td>${user.username}</td>
                <td>${user.firstname}</td>
                <td>${user.lastname}</td>
            </tr>
        </c:forEach>
    </table>
    <h1>Choose currency you need</h1>
    <form name="listForm0" method="post" action="MainServlet">
        <c:forEach items="${currList}" var="curr">
            ${curr.curName}<input type="checkbox" name="check" value="${curr.curAbbreviation}"><br>
            <input type="hidden" name="check" value="false" checked/><br>
        </c:forEach>
        <input type="submit" value="press to look at your choose"><br>
    </form>
</div>
</body>
</html>
