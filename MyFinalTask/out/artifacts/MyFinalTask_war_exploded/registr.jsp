<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="<c:url value="/css/style.css" />" rel="stylesheet">
    <title>Registration</title>
</head>
<body>
<h1>Hello ${username}, we didn't find you in DataBase. Register please.</h1>
<div class="container">
    <img src="https://dwstroy.ru/lessons/les3373/demo/img/men.png">
    <form name="newUserForm" method="post" action="RegistrServlet">
        <div class="dws-input">
            <input type="text" name="username" placeholder="Введите логин">
        </div>
        <div class="dws-input">
            <input type="text" name="firstname" placeholder="Введите имя">
        </div>

        <div class="dws-input">
            <input type="text" name="lastname" placeholder="Введите фамилию">
        </div>

        <div class="dws-input">
            <input type="password" name="password" placeholder="Введите пароль">
        </div>

        <input class="dws-submit" type="submit" name="submit" value="РЕГИСТРАЦИЯ"><br/>
    </form>
</div>
</body>
</html>
