<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="/css/style.css">
    <script src="ajax.js"></script>
</head>
<body>
<div class="containerFull">
    <form id="myForm1" name="listForm0" method="post" action="CurrServlet">
        <h1>UserList of Currency</h1>
        <div class="containerTable">
            <div class="tableRow">
                <span></span>
                <span>CURRENCY NAME</span>
                <span>CURRENCY COUNT</span>
                <span>CURRENCY DATE</span>
                <span>OFFICIAL CURS</span>
            </div>
                <c:forEach items="${userCurrList}" var="cur">
            <div class="tableRow">
                    <div class="tableRow__cell">
                        <input type="checkbox" name="checking" value="${cur.curname}"><br>
                        <input type="hidden" name="checking" value="false" checked/>
                    </div>
                    <div class="tableRow__cell">
                        <span>${cur.curname}</span>
                    </div>
                    <div class="tableRow__cell">
                        <span>${cur.curcount}</span>
                    </div>
                    <div class="tableRow__cell">
                        <span>${cur.curdate}</span>
                    </div>
                    <div class="tableRow__cell">
                        <span>${cur.oficialcurs}</span>
                    </div>
            </div>
                </c:forEach>
        </div>
        <input class="submitMyform1" type="button" value="Del"><br>
    </form>
</div>
</body>
</html>
