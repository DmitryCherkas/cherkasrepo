import dao.CurrencyDAOImpl;
import dao.UserDAOImpl;
import objects.Currency;
import objects.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    CurrencyDAOImpl currencyDAO = new CurrencyDAOImpl();
    private Currency currency = new Currency();
    private List<Currency> currList = currency.makeList();

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserDAOImpl userDAO = new UserDAOImpl();
        User user = new User();
        String rrr = "and now fill out the fields";
        request.setAttribute("rrr", rrr);
        List<User> userList;
        List<String> userCurrency = new ArrayList<>();
        int a = 0;
        userList = userDAO.getUsers();
        request.getSession().setAttribute("userList", userList);
        request.getSession().setAttribute("currList", currList);
        if (username == null && password == null) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
            try {
                requestDispatcher.forward(request, response);
            } catch (ServletException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (userDAO.check(username, password)) {
                    response.sendRedirect("/main.jsp");
                } else {
                    request.getSession().setAttribute("username", username);
                    response.sendRedirect("/registr.jsp");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
