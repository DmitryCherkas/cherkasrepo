package dao;

import connections.DBConnection;
import interfaces.CurrencyDAO;
import objects.CurrencyDB;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CurrencyDAOImpl extends DBConnection implements CurrencyDAO {
    private static final String GET = "SELECT * FROM currency where currency.userid=?";
    private static final String DELETE = "DELETE FROM currency WHERE currency.userid='0'";
    private static final String UPDATE = "UPDATE currency SET currency.userid=?  WHERE currency.cur_id=?";
    private static final String INSERT = "INSERT INTO currency (currency.curname, currency.curcount, currency.oficialcurs, currency.userid, currency.curdate) VALUES (?,?,?,?,?)";

    @Override
    public void addCur(CurrencyDB cur) {
        try (PreparedStatement statement = getConnection().prepareStatement(INSERT)) {
            statement.setString(1, cur.getCurname());
            statement.setInt(2, cur.getCurcount());
            statement.setDouble(3, cur.getOficialcurs());
            statement.setInt(4, cur.getUserid());
            statement.setString(5, cur.getCurdate());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<CurrencyDB> getCur(int userid) throws SQLException {
        List<CurrencyDB> list = new ArrayList<>();
        try (PreparedStatement statement = getConnection().prepareStatement(GET)) {
            statement.setInt(1, userid);
            final ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                CurrencyDB cur = new CurrencyDB();
                cur.setCur_id(Integer.parseInt(rs.getString("cur_id")));
                cur.setCurname(rs.getString("curname"));
                cur.setCurcount(Integer.parseInt(rs.getString("curcount")));
                cur.setOficialcurs((double) (rs.getFloat("oficialcurs")));
                cur.setUserid(Integer.parseInt(rs.getString("userid")));
                cur.setCurdate(rs.getString("curdate"));
                list.add(cur);
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void delCur() {
        try (PreparedStatement statement = getConnection().prepareStatement(DELETE)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(CurrencyDB cur) {
        try (PreparedStatement statement = getConnection().prepareStatement(UPDATE)) {
            statement.setInt(1, cur.getUserid());
            statement.setInt(2, cur.getCur_id());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
