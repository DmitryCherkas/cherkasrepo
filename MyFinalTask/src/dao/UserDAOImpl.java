package dao;

import connections.DBConnection;
import interfaces.UserDAO;
import objects.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAOImpl extends DBConnection implements UserDAO {
    private static final String GET = "SELECT * FROM users";
    private static final String CHECK = "SELECT 1 FROM users where users.username = ? and users.password = ?";
    private static final String INSERT = "INSERT INTO users (users.username, users.password, users.firstname, users.lastname) VALUES (?,?,?,?)";


    @Override
    public void addUser(User user) {
        try (PreparedStatement statement = getConnection().prepareStatement(INSERT)) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFirstname());
            statement.setString(4, user.getLastname());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getUsers() {
        List<User> list = new ArrayList<>();
        try (PreparedStatement statement = getConnection().prepareStatement(GET)) {
            final ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setFirstname(rs.getString("firstname"));
                user.setLastname(rs.getString("lastname"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                list.add(user);
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public User takeHim(String login){
        String myGET;
        User user = new User();
        myGET = "SELECT * FROM users where users.username = ?";
        try (PreparedStatement statement = getConnection().prepareStatement(myGET)) {
            statement.setString(1, login);
            final ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                user.setId(Integer.parseInt(rs.getString("id")));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setFirstname(rs.getString("firstname"));
                user.setLastname(rs.getString("lastname"));
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean check(String login, String password) {
        boolean isUserExists = false;
        try (PreparedStatement ps = getConnection().prepareStatement(CHECK)) {
            ps.setString(1, login);
            ps.setString(2, password);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    isUserExists = true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isUserExists;
    }
}
