package interfaces;

import objects.CurrencyDB;

import java.sql.SQLException;
import java.util.List;

public interface CurrencyDAO {
    void addCur(CurrencyDB cur);
    List<CurrencyDB> getCur(int userid) throws SQLException;
    void delCur();
    void update(CurrencyDB cur);
}
