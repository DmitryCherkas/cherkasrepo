package interfaces;

import objects.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDAO {
    void addUser(User user);
    List<User> getUsers() throws SQLException;
    boolean check(String login, String password);
    User takeHim(String login);
}
