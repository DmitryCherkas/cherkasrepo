package objects;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Currency implements Comparable {
    @SerializedName("Cur_ID")
    @Expose
    private Integer curID;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Cur_Abbreviation")
    @Expose
    private String curAbbreviation;
    @SerializedName("Cur_Scale")
    @Expose
    private Integer curScale;
    @SerializedName("Cur_Name")
    @Expose
    private String curName;
    @SerializedName("Cur_OfficialRate")
    @Expose
    private Double curOfficialRate;
    private boolean cheked;

    public boolean isCheked() {
        return cheked;
    }

    public void setCheked(boolean cheked) {
        this.cheked = cheked;
    }

    private String userDate = "";

    public String getUserDate() {
        return userDate;
    }

    public void setUserDate(String userDate) {
        this.userDate = userDate;
    }

    public Integer getCurID() {
        return curID;
    }

    public void setCurID(Integer curID) {
        this.curID = curID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCurAbbreviation() {
        return curAbbreviation;
    }

    public void setCurAbbreviation(String curAbbreviation) {
        this.curAbbreviation = curAbbreviation;
    }

    public Integer getCurScale() {
        return curScale;
    }

    public void setCurScale(Integer curScale) {
        this.curScale = curScale;
    }

    public String getCurName() {
        return curName;
    }

    public void setCurName(String curName) {
        this.curName = curName;
    }

    public Double getCurOfficialRate() {
        return curOfficialRate;
    }

    public void setCurOfficialRate(Double curOfficialRate) {
        this.curOfficialRate = curOfficialRate;
    }

    public String getInformation(){
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";
        String apiAdress = "http://www.nbrb.by/API/ExRates/Rates?Periodicity=0";
        try {
            URL url = new URL(apiAdress);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            resultJson = buffer.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultJson;
    }

    public List<Currency> makeList(){
        String json = "";
        Currency currency = new Currency();

        json += currency.getInformation();

        Type listType = new TypeToken<List<Currency>>() {}.getType();
        List<Currency> result = new Gson().fromJson(json, listType);
        //Collections.sort(result);

        return result;
    }

    public boolean checkObject(List<String> userCurrency, String abbreviation){
        boolean check = false;
        for (String s : userCurrency) {
            if (abbreviation.equals(s)) {
                check = true;
                break;
            } else {
                check = false;
            }
        }
        return  check;
    }


    public List<CurrencyDB> makeListForDataBase(List<String> userCurrency, User user){
        Currency currency = new Currency();
        boolean check = false;
        List<CurrencyDB> list = new ArrayList<>();
        List<Currency> list1 = new ArrayList<>(makeList());
        for (Currency aList1 : list1) {
            CurrencyDB cur = new CurrencyDB();
            cur.setCurname(aList1.getCurAbbreviation());
            cur.setCurcount(aList1.getCurScale());
            cur.setOficialcurs(aList1.getCurOfficialRate());
            check = currency.checkObject(userCurrency, aList1.curAbbreviation);
            if(check) cur.setUserid(user.getId());
            else cur.setUserid(0);
            cur.setCurdate(aList1.getDate());
            list.add(cur);
        }
        return list;
    }

    @Override
    public int compareTo(Object o) {
        Currency currency = (Currency)o;
        return this.getCurAbbreviation().compareTo(currency.getCurAbbreviation());
    }
}

