package objects;

public class CurrencyDB {
    private int cur_id;
    private String curname;
    private int curcount;
    private Double oficialcurs;
    private int userid;
    private String curdate;

    public CurrencyDB() {
    }

    public CurrencyDB(int cur_id, String curname, int curcount, Double oficialcurs, int userid, String curdate) {
        this.cur_id = cur_id;
        this.curname = curname;
        this.curcount = curcount;
        this.oficialcurs = oficialcurs;
        this.userid = userid;
        this.curdate = curdate;
    }

    public int getCur_id() {
        return cur_id;
    }

    public void setCur_id(int cur_id) {
        this.cur_id = cur_id;
    }

    public String getCurname() {
        return curname;
    }

    public void setCurname(String curname) {
        this.curname = curname;
    }

    public int getCurcount() {
        return curcount;
    }

    public void setCurcount(int curcount) {
        this.curcount = curcount;
    }

    public Double getOficialcurs() {
        return oficialcurs;
    }

    public void setOficialcurs(Double oficialcurs) {
        this.oficialcurs = oficialcurs;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getCurdate() {
        return curdate;
    }

    public void setCurdate(String curdate) {
        this.curdate = curdate;
    }
}

