package servlets;

import dao.CurrencyDAOImpl;
import objects.CurrencyDB;
import objects.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/CurrServlet")
public class CurrServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        CurrencyDAOImpl currencyDAO = new CurrencyDAOImpl();
        User user = (User) request.getSession().getAttribute("user");
        String checkerValue = request.getParameter("checker");
        if (checkerValue == null){
            List<String> list = Arrays.asList(request.getParameterValues("checking"));
            List<CurrencyDB> userCurrList1 = (List<CurrencyDB>) request.getSession().getAttribute("userCurrList");
            for (CurrencyDB aCurr : userCurrList1) {
                for (String s : list) {
                    if (aCurr.getCurname().equals(s)) {
                        aCurr.setUserid(0);
                        currencyDAO.update(aCurr);
                        System.out.println("AAAAAAAAAAAAAAAAAAAAAAA");
                    }
                }
            }
            try {
                List<CurrencyDB> userCurrList11 = currencyDAO.getCur(user.getId());
                request.getSession().setAttribute("userCurrList", userCurrList11);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            RequestDispatcher dispatcher1 = request.getRequestDispatcher("/tableCyrr.jsp");
            try {
                dispatcher1.forward(request, response);
            } catch (ServletException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            if (checkerValue.equals("true")) {
                currencyDAO.delCur();
                try {
                    response.sendRedirect("/login.jsp");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/tableCyrr.jsp");
                try {
                    dispatcher.forward(request, response);
                } catch (ServletException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
