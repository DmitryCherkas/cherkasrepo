package servlets;

import dao.CurrencyDAOImpl;
import dao.UserDAOImpl;
import objects.Currency;
import objects.CurrencyDB;
import objects.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        CurrencyDAOImpl currencyDAO = new CurrencyDAOImpl();
        Currency currency = new Currency();
        User user = (User) request.getSession().getAttribute("user");
        //List<Currency> currList = (List<Currency>) request.getSession().getAttribute("currList");
        List<String> list = Arrays.asList(request.getParameterValues("check"));
        List<String> userCurrency = new ArrayList<>();
        for (String aList : list) {
            if (!aList.equals("false")) {
                userCurrency.add(aList);
            }
        }
        List<CurrencyDB> currList = currency.makeListForDataBase(userCurrency, user);
        for (CurrencyDB acurrList : currList) {
            currencyDAO.addCur(acurrList);
            //System.out.println("table" + acurrList.getOficialcurs());
        }
        try {
            List<CurrencyDB> userCurrList = currencyDAO.getCur(user.getId());
            request.getSession().setAttribute("userCurrList", userCurrList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            response.sendRedirect("/curr.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}