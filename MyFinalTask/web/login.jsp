<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored = "false"%>
<html>
<head>
    <link href="<c:url value="/css/style.css" />" rel="stylesheet">
    <title>Authorization</title>
</head>
<body>
<div class="container">
    <img src="https://dwstroy.ru/lessons/les3373/demo/img/men.png">
    <form name="loginForm" method="post" action="loginServlet">
        <div class="dws-input">
            <input type="text" name="username" placeholder="Введите логин">
        </div>
        <div class="dws-input">
            <input type="password" name="password" placeholder="Введите пароль">
        </div>
        <input class="dws-submit" type="submit" name="submit" value="ВОЙТИ"><br />
    </form>
</div>
</body>