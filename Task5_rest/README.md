# Distributed web service
This project consists of six nodes which are differ only in their ports and database schemes. 
You can use any of them to operate information which database schemes have.

Here is one of the nodes, it is the rest web service which works like KV storage database and
use MySQL database as primary storage.

## Entities:
* **Collections** (name, cache limit, algorithm, json scheme)
* **Documents**  (key (String), value (JSON))

## Actions:
* **Collections**  create, delete, get (all fields), update (name, cache limit, algo), list 
(shows all fields except JSON schema)
* **Documents** CRUD + list

There were used Spring boot MVC in project, and Postman for checking.
