package project.DAOImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import project.entities.Documents;
import project.interfaces.DocumentsDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class DocumentsDAOImpl implements DocumentsDAO {

    @PersistenceUnit
    private EntityManagerFactory managerFactory;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createDocument(Documents document){
        entityManager.persist(document);
        entityManager.flush();
    }

    @Override
    public Documents getDocumentByName(String  documentName){
        return entityManager.find(Documents.class, documentName);
    }

    @Override
    public List<Documents> list(String lastElementName, int maxResults){
        Query q = null;
        if (lastElementName.equals("non")){
            q = (Query) entityManager.createQuery("from Documents", Documents.class);
            q.setFirstResult(0);
            q.setMaxResults(maxResults);
        }
        else {
            q = (Query) entityManager.createQuery("from Documents" +
                    " where name > :paramName order by name ASC", Documents.class);
            q.setParameter("paramName", lastElementName);
            q.setMaxResults(maxResults);
        }
        return q.getResultList();
    }

    @Override
    public void updateDocument(String name, String newName, String json, String collection_name){
        Documents document = entityManager.find(Documents.class, name);
        Documents newDocument = new Documents();
        newDocument.setName(newName);
        newDocument.setJson(json);
        newDocument.setCollection_name(collection_name);
        entityManager.remove(document);
        entityManager.persist(newDocument);
        entityManager.flush();
    }

    @Override
    public void deleteDocument(String documentName){
        Documents document = entityManager.find(Documents.class, documentName);
        entityManager.remove(document);
        entityManager.flush();
    }
}
