package project.DAOImpl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import project.entities.MyCollections;
import project.entities.SmallMyCollections;
import project.interfaces.MyCollectionsDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class MyCollectionsDAOImpl implements MyCollectionsDAO {

    @PersistenceUnit
    private EntityManagerFactory managerFactory;

    @PersistenceContext
    private EntityManager entityManager;

    public void createMyCollection(MyCollections collection){
        entityManager.persist(collection);
        entityManager.flush();
    }

    @Override
    public MyCollections getMyCollectionByName(String collectionName) {
        return entityManager.find(MyCollections.class, collectionName);
    }

    @Override
    public List<SmallMyCollections> list(String lastElementName, int maxResults) {
        Query q = null;
        if (lastElementName.equals("non")){
            q = (Query) entityManager.createQuery("from SmallMyCollections", SmallMyCollections.class);
            q.setFirstResult(0);
            q.setMaxResults(maxResults);
        }
        else {
            q = (Query) entityManager.createQuery("from SmallMyCollections" +
                    " where nameCollection > :paramName order by nameCollection ASC", SmallMyCollections.class);
            q.setParameter("paramName", lastElementName);
            q.setMaxResults(maxResults);
        }
        return q.getResultList();
    }

    @Override
    public void updateMyCollection(String name, String newName, int limit, String algorithm) {
        MyCollections collection = entityManager.find(MyCollections.class, name);
        MyCollections newCollection = new MyCollections();
        newCollection.setName(newName);
        newCollection.setLimit(limit);
        newCollection.setAlgorithm(algorithm);
        newCollection.setScheme(collection.getScheme());
        entityManager.remove(collection);
        entityManager.persist(newCollection);
        entityManager.flush();
    }

    @Override
    public void deleteMyCollection(String collectionName) {
        MyCollections collection = entityManager.find(MyCollections.class, collectionName);
        entityManager.remove(collection);
        entityManager.flush();
    }
}
