package project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task5SpringApplication.class, args);
    }
}
