package project.caches;

import project.interfaces.CacheInterface;

import java.util.LinkedHashMap;
import java.util.Map;

public class LFUCacheImpl<K, V> implements CacheInterface<K, V> {
    //Define Node
    static class Node {
        private Object value;
        private int frequency;
        //Default constructor
        public Node() {}

        public Object getvalue() {
            return value;
        }
        public void setvalue(Object value) {
            this.value = value;
        }

        public int getFrequency() {
            return frequency;
        }
        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }
    }

    private int maxSize;
    private static LinkedHashMap<Object, Node> cache = new LinkedHashMap<Object, Node>();

    public LFUCacheImpl(int maxSize) {
        this.maxSize = maxSize;
    }

    public K getLFUKey(){
        K key = null;
        int minFreq = Integer.MAX_VALUE;
        for(Map.Entry<Object, Node> entry : cache.entrySet()) {
            if(minFreq > entry.getValue().frequency) {
                key = (K) entry.getKey();
                minFreq = entry.getValue().frequency;
            }
        }
        return key;
    }

    public boolean isFull() {
        return size() == maxSize;

    }

    public V get(K key) {
        if(contains(key)) {
            Node myNode = cache.get(key);
            myNode.frequency++;
            cache.put(key, myNode);
            return (V) myNode.value;
        }
        return null;
    }

    public void put(K key, V value) {
        if(contains(key)){
            get(key);
            return;
        }
        if(!isFull()) {
            Node myNode = new Node();
            myNode.setvalue(value);
            myNode.setFrequency(0);
            cache.put(key, myNode);
        }
        else {
            K entryKeyToBeRemoved = getLFUKey();
            remove(entryKeyToBeRemoved);
            Node myNode = new Node();
            myNode.setvalue(value);
            myNode.setFrequency(0);
            cache.put(key, myNode);
        }
    }

    public void remove(K key) {
        cache.remove(key);
    }

    public boolean contains(Object key) {
        return cache.containsKey(key);
    }

    public V putIfAbsent(K key, V value) {
        Node myNode = new Node();
        if (!cache.containsKey(key)) {
            myNode.setvalue(value);
            myNode.setFrequency(0);
            cache.put(key, myNode);
            return (V) myNode.value;
        }
        else { return get(key); }
    }

    public Integer size() {
        return cache.size();
    }
}
