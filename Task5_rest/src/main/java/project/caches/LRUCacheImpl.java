package project.caches;

import project.interfaces.CacheInterface;

import java.util.HashMap;

public class LRUCacheImpl<K, V> implements CacheInterface<K, V> {
    //Node with key, value pair and pointers to the previous and next items
    static class Node<T, U> {
        Node<T, U> previous;
        Node<T, U> next;
        Object key;
        Object value;
        //Default constructor
        public Node(Node<T, U> previous, Node<T, U> next, Object key, Object value){
            this.previous = previous;
            this.next = next;
            this.key = key;
            this.value = value;
        }
    }

    private HashMap<Object, Node<K, V>> cache;
    private Node<K, V> leastRecentlyUsed;
    private Node<K, V> mostRecentlyUsed;
    private int maxSize;
    private int currentSize;

    public LRUCacheImpl(int maxSize){
        this.maxSize = maxSize;
        this.currentSize = 0;
        leastRecentlyUsed = new Node<K, V>(null, null, null, null);
        mostRecentlyUsed = leastRecentlyUsed;
        cache = new HashMap<Object, Node<K, V>>();
    }

    public V get(K key) {
        Node<K, V> tempNode = cache.get(key);
        if (tempNode == null){
            return null;
        }
        //If at the right-mode, leave the list as it is
        else if (tempNode.key.equals(mostRecentlyUsed.key)){
            return (V) mostRecentlyUsed.value;
        }
        //Get the next and previous nodes
        Node<K, V> nextNode = tempNode.next;
        Node<K, V> previousNode = tempNode.previous;
        //If at the left-most, we update list
        if (tempNode.key.equals(leastRecentlyUsed.key)){
            nextNode.previous = null;
            leastRecentlyUsed = nextNode;
        }
        //If at the middle, we need to update the items before and after our item
        else if (!tempNode.key.equals(mostRecentlyUsed.key)){
            previousNode.next = nextNode;
            nextNode.previous = previousNode;
        }
        //Then move our item to the MRU
        tempNode.previous = mostRecentlyUsed;
        mostRecentlyUsed.next = tempNode;
        mostRecentlyUsed = tempNode;
        mostRecentlyUsed.next = null;
        return (V) tempNode.value;
    }

    public void put(K key, V value) {
        if(contains(key)){
            get(key);
            return;
        }
        Node<K, V> myNode = new Node<K, V>(mostRecentlyUsed, null, key, value);
        //Put the new node at the right-most end of the linked-list
        mostRecentlyUsed.next = myNode;
        cache.put(key, myNode);
        mostRecentlyUsed = myNode;
        //Delete the left-most entry and update the LRU pointer
        if (size() == maxSize){
            remove((K) leastRecentlyUsed.key);
            leastRecentlyUsed = leastRecentlyUsed.next;
            leastRecentlyUsed.previous = null;
        }
        //Update cache size, for the first added entry update the LRU pointer
        else if (size() < maxSize){
            if (size() == 0){
                leastRecentlyUsed = myNode;
            }
            currentSize++;
        }
    }

    public void remove(K key) {
        cache.remove(key);
    }

    public boolean contains(K key) {
        return cache.containsKey(key);
    }

    public V putIfAbsent(K key, V value) {
        Node<K, V> myNode = new Node<K, V>(mostRecentlyUsed, null, key, value);
        if (!cache.containsKey(key)) {
            mostRecentlyUsed.next = myNode;
            cache.put(key, myNode);
            mostRecentlyUsed = myNode;
        }
        return (V) myNode.value;
    }

    public Integer size() {
        return currentSize;
    }
}

