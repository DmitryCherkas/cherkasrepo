package project.controllers;

import org.springframework.http.HttpStatus;
import project.entities.Documents;
import project.interfaces.DocumentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.storage.DocumentsDistributedStorage;

import java.util.List;

@RestController
public class DocumentsController {

    @Autowired
    private DocumentsService documentsService;

    @Autowired
    private DocumentsDistributedStorage storage;

    @PostMapping("/document")
    public ResponseEntity<Documents> addDocument(@RequestBody Documents document) {

        storage.addDocument(document);

        return new ResponseEntity<>(document, HttpStatus.CREATED);
    }

    @DeleteMapping("/document/{documentName}/{collectionName}")
    public ResponseEntity<?> deleteDocument(@PathVariable("documentName") String documentName,
                                            @PathVariable("collectionName") String collectionName) {

        storage.deleteDocument(documentName, collectionName);

        return ResponseEntity.ok(documentName);
    }

    @PutMapping("/document/{newName}")
    public ResponseEntity<?> updateDocument(@RequestBody Documents document, @PathVariable("newName") String newName) {

        String name = document.getName();
        String json = document.getJson();
        String collection_name = document.getCollection_name();

        storage.updateDocument(name, newName, json, collection_name);

        return ResponseEntity.ok(newName);
    }

    @GetMapping("/document/{documentName}/{collectionName}")
    public ResponseEntity<Documents> getDocumentByName(@PathVariable("documentName") String documentName,
                                               @PathVariable("collectionName") String collectionName) {

        storage.getDocumentByName(documentName, collectionName);

        return ResponseEntity.ok(storage.getDocumentByName(documentName, collectionName));
    }

    @GetMapping("/document/list/{lastElementName}/{maxResults}")
    public ResponseEntity<List<Documents>> list(@PathVariable("lastElementName")String lastElementName,@PathVariable("maxResults") int maxResults) {

        List<Documents> listOfDocuments = storage.list(lastElementName, maxResults);

        return ResponseEntity.ok(listOfDocuments);
    }

    //for node
    @PostMapping("/documentForNode")
    public ResponseEntity<Documents> addDocumentForNode(@RequestBody Documents document) {

        documentsService.createDocument(document);

        return new ResponseEntity<>(document, HttpStatus.CREATED);
    }

    @DeleteMapping("/documentForNode/{documentName}/{collectionName}")
    public ResponseEntity<?> deleteDocumentForNode(@PathVariable("documentName") String documentName,
                                                   @PathVariable("collectionName") String collectionName) {

        documentsService.deleteDocument(documentName, collectionName);

        return ResponseEntity.ok(documentName);
    }

    @PutMapping("/documentForNode/{name}/{newName}/{json}/{collection_name}")
    public ResponseEntity<?> updateDocumentForNode(@PathVariable("name")String name, @PathVariable("newName")String newName,
                                                   @PathVariable("json")String json, @PathVariable("collection_name")String collection_name) {

        documentsService.updateDocument(name, newName, json, collection_name);

        return ResponseEntity.ok(newName);
    }

    @GetMapping("/documentForNode/{documentName}/{collectionName}")
    public ResponseEntity<Documents> getDocumentByNameForNode(@PathVariable("documentName") String documentName,
                                                              @PathVariable("collectionName") String collectionName) {

        documentsService.getDocumentByName(documentName, collectionName);

        return ResponseEntity.ok(documentsService.getDocumentByName(documentName, collectionName));
    }

    @GetMapping("/documentForNode/list/{lastElementName}/{maxResults}")
    public ResponseEntity<List<Documents>> listForNode(@PathVariable("lastElementName")String lastElementName,@PathVariable("maxResults") int maxResults) {

        List<Documents> listOfDocuments = documentsService.list(lastElementName, maxResults);

        return ResponseEntity.ok(listOfDocuments);
    }
}
