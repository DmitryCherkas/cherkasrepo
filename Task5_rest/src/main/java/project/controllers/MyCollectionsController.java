package project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.entities.MyCollections;
import project.entities.SmallMyCollections;
import project.interfaces.MyCollectionsService;
import project.storage.MyCollectionsDistributedStorage;

import java.util.List;

@RestController
public class MyCollectionsController {

    @Autowired
    private MyCollectionsDistributedStorage storage;

    @Autowired
    private MyCollectionsService myCollectionsService;

    @PostMapping("/mycollection")
    public ResponseEntity<MyCollections> addMyCollection(@RequestBody MyCollections collection) {

        if(collection == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        storage.addMyCollection(collection);

        return new ResponseEntity<>(collection, HttpStatus.CREATED);
    }

    @DeleteMapping("/mycollection/{name}")
    public ResponseEntity<String> deleteCollection(@PathVariable("name") String  collectionName) {

        storage.deleteMyCollection(collectionName);

        return new ResponseEntity<>(collectionName, HttpStatus.OK);
    }

    @PutMapping("/mycollection/{name}/{newName}/{limit}/{algorithm}")
    public ResponseEntity<String> updateCollection(@PathVariable("name") String name,@PathVariable("newName") String newName,
                                              @PathVariable("limit") int limit, @PathVariable("algorithm") String algorithm) {

        storage.updateCollection(name, newName, limit, algorithm);

        return new ResponseEntity<>(newName, HttpStatus.OK);
    }

    @GetMapping("/mycollection/{name}")
    public ResponseEntity<MyCollections> getCollectionByName(@PathVariable("name")String collectionName) {

        return ResponseEntity.ok(storage.getMyCollectionByName(collectionName));
    }

    @GetMapping("/mycollection/{lastElementName}/{maxResults}")
    public ResponseEntity<List<SmallMyCollections>> list(@PathVariable("lastElementName")String lastElementName, @PathVariable("maxResults") int maxResults) {

        List<SmallMyCollections> listOfCollections = storage.list(lastElementName, maxResults);

        return ResponseEntity.ok(listOfCollections);
    }

    //for node
    @PostMapping("/mycollectionForNode")
    public ResponseEntity<MyCollections> addMyCollectionForNode(@RequestBody MyCollections collection) {

        if(collection == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        myCollectionsService.createMyCollection(collection);

        return new ResponseEntity<>(collection, HttpStatus.CREATED);
    }

    @DeleteMapping("/mycollectionForNode/{name}")
    public ResponseEntity<String> deleteCollectionForNode(@PathVariable("name") String  collectionName) {

        myCollectionsService.deleteMyCollection(collectionName);

        return new ResponseEntity<>(collectionName, HttpStatus.OK);
    }

    @PutMapping("/mycollectionForNode/{name}/{newName}/{limit}/{algorithm}")
    public ResponseEntity<String> updateCollectionForNode(@PathVariable("name") String name,@PathVariable("newName") String newName,
                                                          @PathVariable("limit") int limit, @PathVariable("algorithm") String algorithm) {

        myCollectionsService.updateMyCollection(name, newName, limit, algorithm);

        return new ResponseEntity<>(newName, HttpStatus.OK);
    }

    @GetMapping("/mycollectionForNode/{name}")
    public ResponseEntity<MyCollections> getCollectionByNameForNode(@PathVariable("name")String collectionName) {

        return ResponseEntity.ok(myCollectionsService.getMyCollectionByName(collectionName));
    }

    @GetMapping("/mycollectionForNode/{lastElementName}/{maxResults}")
    public ResponseEntity<List<SmallMyCollections>> listForNode(@PathVariable("lastElementName")String lastElementName, @PathVariable("maxResults") int maxResults) {

        List<SmallMyCollections> listOfCollections = myCollectionsService.list(lastElementName, maxResults);

        return ResponseEntity.ok(listOfCollections);
    }
}
