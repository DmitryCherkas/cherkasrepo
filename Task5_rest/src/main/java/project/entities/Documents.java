package project.entities;

import javax.persistence.*;

@Entity
@Table(name = "documents")
public class Documents  {

    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "json")
    private String json;

    @Column(name = "collection_name")
    private String collection_name;

    public Documents() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getCollection_name() {
        return collection_name;
    }

    public void setCollection_name(String collection_name) {
        this.collection_name = collection_name;
    }
}
