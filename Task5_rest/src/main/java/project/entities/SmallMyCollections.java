package project.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "my")
public class SmallMyCollections {

    @Id
    @Column(name = "name")
    private String nameCollection;

    @Column(name = "cache_limit")
    private int limit;

    @Column(name = "cache_algoritm")
    private String algorithm;

    public SmallMyCollections() {
    }

    public String getName() {
        return nameCollection;
    }

    public void setName(String name) {
        this.nameCollection = name;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }
}