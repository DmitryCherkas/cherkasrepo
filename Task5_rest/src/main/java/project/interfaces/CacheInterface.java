package project.interfaces;

public interface CacheInterface<K, V> {

    V get(K key);

    void put(K key, V value);

    void remove(K key);

    boolean contains(K key);

    V putIfAbsent(K key, V value);

    Integer size();
}
