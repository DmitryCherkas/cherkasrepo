package project.interfaces;

import project.entities.Documents;

import java.util.List;

public interface DocumentsService {

    void createDocument(Documents document);

    Documents getDocumentByName(String documentName, String collectionName);

    List<Documents> list(String lastElementName, int maxResults);

    void updateDocument(String name, String newName, String json, String collection_name);

    void deleteDocument(String documentName, String collectionName);
}
