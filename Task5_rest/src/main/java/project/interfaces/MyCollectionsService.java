package project.interfaces;

import project.entities.MyCollections;
import project.entities.SmallMyCollections;

import java.util.List;

public interface MyCollectionsService {

    void createMyCollection(MyCollections collection);

    MyCollections getMyCollectionByName(String  collectionName);

    List<SmallMyCollections> list(String lastElementName, int maxResults);

    void updateMyCollection(String name, String newName, int limit, String algorithm);

    void deleteMyCollection(String collectionName);
}
