package project.servicesImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.caches.LFUCacheImpl;
import project.caches.LRUCacheImpl;
import project.entities.Documents;
import project.entities.MyCollections;
import project.interfaces.CacheInterface;
import project.interfaces.MyCollectionsDAO;
import project.interfaces.DocumentsDAO;
import project.interfaces.DocumentsService;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DocumentsServiceImpl implements DocumentsService {

    @Autowired
    private DocumentsDAO documentsDAO;

    @Autowired
    private MyCollectionsDAO myCollectionsDAO;

    private Map<String, CacheInterface<String, Documents>> cacheMap = new HashMap<>();

    private static Logger logger  = LoggerFactory.getLogger(DocumentsServiceImpl.class);

    private boolean checkValidation(String jsonScheme, String json){
        JSONObject jsonSchema = new JSONObject(jsonScheme);
        JSONObject jsonSubject = new JSONObject(json);
        Schema schema = SchemaLoader.load(jsonSchema);
        boolean valid;
        try{
            schema.validate(jsonSubject);
            valid = true;
        } catch (ValidationException e){
            valid = false;
        }
        return valid;
    }

    public CacheInterface<String, Documents> getCache(String algorithm, int limit) {

        if (algorithm.equals("lfu")) { return new LFUCacheImpl<>(limit); }
        if (algorithm.equals("lru")) { return new LRUCacheImpl<>(limit); }
        return null;
    }

    @Transactional
    @Override
    public void createDocument(Documents document){
        MyCollections collection = myCollectionsDAO.getMyCollectionByName(document.getCollection_name());
        if(checkValidation(collection.getScheme(), document.getJson())) {
            documentsDAO.createDocument(document);
            cacheMap.putIfAbsent(collection.getName(), getCache(collection.getAlgorithm(), collection.getLimit()));
            CacheInterface<String, Documents> documentsCache = cacheMap.get(collection.getName());
            documentsCache.put(document.getName(), document);
        }
        else logger.info("invalid JSON");
    }

    @Transactional
    @Override
    public Documents getDocumentByName(String  documentName, String  collectionName){
        if(cacheMap.containsKey(collectionName)){
            CacheInterface<String, Documents> documentsCache = cacheMap.get(collectionName);
            if(documentsCache.contains(documentName)){
                return documentsCache.get(documentName);
            }
        }
        return documentsDAO.getDocumentByName(documentName);
    }

    @Transactional
    @Override
    public List<Documents> list(String lastElementName, int maxResults){
        return documentsDAO.list(lastElementName, maxResults);
    }

    @Transactional
    @Override
    public void updateDocument(String name, String newName, String json, String collection_name){
        Documents document = documentsDAO.getDocumentByName(name);
        MyCollections collection = myCollectionsDAO.getMyCollectionByName(document.getCollection_name());
        if (!cacheMap.containsKey(collection.getName())) {
            cacheMap.put(collection.getName(), getCache(collection.getAlgorithm(), collection.getLimit()));
        }
        else {
            CacheInterface<String, Documents> documentsCache = cacheMap.get(collection.getName());
            if(documentsCache.contains(name)) documentsCache.remove(name);
        }
        if(checkValidation(collection.getScheme(), json)) {
            documentsDAO.updateDocument(name, newName, json, collection_name);
            CacheInterface<String, Documents> documentsCache = cacheMap.get(collection.getName());
            documentsCache.put(newName, documentsDAO.getDocumentByName(newName));
        }
        else logger.info("invalid JSON");
    }

    @Transactional
    @Override
    public void deleteDocument(String documentName, String collectionName){
        documentsDAO.deleteDocument(documentName);
        if(cacheMap.containsKey(collectionName)) {
            CacheInterface<String, Documents> documentsCache = cacheMap.get(collectionName);
            if (documentsCache.contains(documentName)) documentsCache.remove(documentName);
        }
    }
}
