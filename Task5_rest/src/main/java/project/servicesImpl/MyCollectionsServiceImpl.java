package project.servicesImpl;

import project.entities.Documents;
import project.entities.MyCollections;
import project.entities.SmallMyCollections;
import project.interfaces.DocumentsDAO;
import project.interfaces.MyCollectionsDAO;
import project.interfaces.MyCollectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MyCollectionsServiceImpl implements MyCollectionsService {

    @Autowired
    private MyCollectionsDAO myCollectionsDAO;

    @Autowired
    private DocumentsDAO documentsDAO;

    @Transactional
    @Override
    public void createMyCollection(MyCollections collection){
        myCollectionsDAO.createMyCollection(collection);
    }

    @Transactional
    @Override
    public MyCollections getMyCollectionByName(String  collectionName){
        return myCollectionsDAO.getMyCollectionByName(collectionName);
    }

    @Transactional
    @Override
    public List<SmallMyCollections> list(String lastElementName, int maxResults){
        return myCollectionsDAO.list(lastElementName, maxResults);
    }

    @Transactional
    @Override
    public void updateMyCollection(String name, String newName, int limit, String algorithm){
        myCollectionsDAO.updateMyCollection(name, newName, limit, algorithm);
    }

    @Transactional
    @Override
    public void deleteMyCollection(String collectionName){
        myCollectionsDAO.deleteMyCollection(collectionName);
        List<Documents> documentsList = documentsDAO.list("non", 1000000000);
        for (Documents document : documentsList) {
            if (document.getCollection_name().equals(collectionName)) {
                documentsDAO.deleteDocument(document.getName());
            }
        }
    }
}
