package project.storage;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import project.entities.Documents;
import project.interfaces.DocumentsDAO;
import project.interfaces.DocumentsService;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Repository
public class DocumentsDistributedStorage {

    @Autowired
    private DocumentsService documentsService;

    @Autowired
    private DocumentsDAO documentsDAO;

    protected int selfNode;
    protected Map<Integer, List<String>> nodesList;
    private static Logger logger  = LoggerFactory.getLogger(MyCollectionsDistributedStorage.class);

    public int getSelfNode(){
        for (Map.Entry<Integer, List<String>> entry : nodesList.entrySet()){
            int factor = Integer.parseInt(String.valueOf(entry.getKey()));
            List<String> list = entry.getValue();
            for (String item : list){ if ("selfNode".equals(item)){ selfNode = factor; } }
        }
        return selfNode;
    }

    public Map<Integer, List<String>> getNodesList(){
        JsonParser parser = new JsonParser();
        JsonElement jsonObj = null;
        try {
            jsonObj = parser.parse(new FileReader("src/main/resources/nodeList.json"));
            nodesList  = new HashMap<>(new Gson().fromJson(jsonObj.toString(),Map.class));
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        }
        return nodesList;
    }

    public Documents getDocumentByName(String documentName, String collectionName){
        nodesList = getNodesList();
        selfNode = getSelfNode();
        Documents document = documentsService.getDocumentByName(documentName, collectionName);
        if (document != null) { return document; }
        else {
            for(int i = 0; i < nodesList.size(); i++){
                if(i != selfNode) {
                    String number = String.valueOf(i);
                    List<String> nodes = nodesList.get(number);
                    for (String node : nodes) {
                        URI uri;
                        try { uri = new URI("http://" + node + "/documentForNode/" + documentName + "/" + collectionName);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers = new HttpHeaders();
                        headers.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
                        RestTemplate restTemplate = new RestTemplate();
                        ResponseEntity<Documents> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Documents.class);
                        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                            logger.info("document was got from : {}", node);
                            return responseEntity.getBody();
                        }
                    }
                }
            }
        }
        logger.info("document doesn't exist");
        return null;
    }

    public void addDocument(Documents document){
        nodesList = getNodesList();
        selfNode = getSelfNode();
        Documents check = null;
        for(int i = 0; i < nodesList.size(); i++) {
            if (i != selfNode) {
                String number = String.valueOf(i);
                List<String> nodes = nodesList.get(number);
                for (String node : nodes) {
                    URI uri;
                    try {
                        uri = new URI("http://" + node + "/documentForNode/" + document.getName() + "/" + document.getCollection_name());
                    } catch (URISyntaxException e) {
                        logger.error("URISyntaxException", e);
                        continue;
                    }
                    HttpHeaders headers = new HttpHeaders();
                    headers.add("Content-Type", "application/json");
                    HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
                    RestTemplate restTemplate = new RestTemplate();
                    ResponseEntity<Documents> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Documents.class);
                    if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                        check = responseEntity.getBody();
                    }
                    if(check != null){
                        logger.info("Document is already exist in : {}", node);
                        return;
                    }
                }
            }
        }
        String number = String.valueOf(getSelfNode());
        List<String> nodes = nodesList.get(number);
        for (String node : nodes) {
            if ("selfNode".equals(node)) {
                documentsService.createDocument(document);
                logger.info("document was created");
            } else { create(node, document); }
        }
        if(!checkForCreate(nodes, document)) rollbackForCreate(nodes, document);
    }

    public void deleteDocument(String documentName, String collectionName){
        Documents document = documentsService.getDocumentByName(documentName ,collectionName);
        nodesList = getNodesList();
        String number = String.valueOf(getSelfNode());
        if(document != null){
            List<String> nodes = nodesList.get(number);
            for (String node : nodes) {
                if ("selfNode".equals(node)) {
                    documentsService.deleteDocument(documentName, collectionName);
                    logger.info("document was delete");
                } else { delete(documentName, collectionName, node); }
            }
            if(!check(nodes, documentName, collectionName)){ rollbackForDelete(nodes, documentName, collectionName, document);}
        }
        else {
            for (int i = 0; i < nodesList.size(); i++) {
                if (i != selfNode) {
                    String num = String.valueOf(i);
                    List<String> nodes = nodesList.get(num);
                    Documents check = null;
                    for (String node : nodes) {
                        URI uri2;
                        try { uri2 = new URI("http://" + node + "/documentForNode/" + documentName + "/" + collectionName);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers2 = new HttpHeaders();
                        headers2.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                        RestTemplate restTemplate2 = new RestTemplate();
                        ResponseEntity<Documents> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, Documents.class);
                        if (responseEntity2 != null && responseEntity2.getStatusCode() == HttpStatus.OK) {
                            logger.info("document was got from : {} for checking", node);
                            check = responseEntity2.getBody();
                        }
                        if(check != null){ delete(documentName, collectionName, node); }
                    }
                    if(check != null) if(!check(nodes, documentName, collectionName)){ rollbackForDelete(nodes, documentName, collectionName, check);}
                }
            }
        }
    }

    public void updateDocument(String name, String newName, String json, String collection_name) {
        nodesList = getNodesList();
        String number = String.valueOf(getSelfNode());
        Documents document = documentsDAO.getDocumentByName(name);
        if(document != null){
            List<String> nodes = nodesList.get(number);
            for (String node : nodes) {
                if ("selfNode".equals(node)) {
                    documentsService.updateDocument(name, newName, json, collection_name);
                    logger.info("document was updated");
                } else {
                    update(name, newName, json, collection_name, node);
                }
            }
            if(!check(nodes, document.getName(), document.getCollection_name())) { rollbackForUpdate(nodes, name, newName, collection_name, document); }
        }
        else {
            for (int i = 0; i < nodesList.size(); i++) {
                if (i != selfNode) {
                    String num = String.valueOf(i);
                    List<String> nodes = nodesList.get(num);
                    Documents check = null;
                    for (String node : nodes) {
                        URI uri2;
                        try {
                            uri2 = new URI("http://" + node + "/documentForNode/" + name + "/" + collection_name);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers1 = new HttpHeaders();
                        headers1.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity1 = new HttpEntity<>(null, headers1);
                        RestTemplate restTemplate1 = new RestTemplate();
                        ResponseEntity<Documents> responseEntity1 = restTemplate1.exchange(uri2, HttpMethod.GET, httpEntity1, Documents.class);
                        if (responseEntity1 != null && responseEntity1.getStatusCode() == HttpStatus.OK) {
                            check = responseEntity1.getBody();
                            logger.info("document was got from : {} for checking", node);
                        }
                        if (check != null) { update(name, newName, json, collection_name, node); }
                    }
                    assert check != null;
                    if(!check(nodes, check.getName(), check.getCollection_name())) { rollbackForUpdate(nodes, name, newName, collection_name, check); }
                }
            }
        }
    }

    public List<Documents> list(String lastElementName, int maxResults){
        Map<String, Documents> mapList = new HashMap<>();
        nodesList = getNodesList();
        selfNode = getSelfNode();
        for(int i = 0; i < nodesList.size(); i++){
            String number = String.valueOf(i);
            List<String> nodes = nodesList.get(number);
            if(i == selfNode){
                for (String node : nodes) {
                    if("selfNode".equals(node)){
                        List<Documents> list = documentsService.list(lastElementName, maxResults);
                        logger.info("documents were got for list");
                        for (Documents aList : list) {
                            mapList.put(aList.getName(), aList);
                        }
                    }
                    else {
                        URI uri;
                        try {
                            uri = new URI("http://" + node + "/documentForNode/list/" + lastElementName + "/" + maxResults);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers = new HttpHeaders();
                        headers.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
                        RestTemplate restTemplate = new RestTemplate();
                        ResponseEntity<List<Documents>> responseEntity1 = restTemplate.exchange(uri, HttpMethod.GET, httpEntity,
                                new ParameterizedTypeReference<List<Documents>>() {});
                        List<Documents> list = responseEntity1.getBody();
                        logger.info("documents were got for list from : {}", node);
                        assert list != null;
                        for (Documents item : list){
                            mapList.put(item.getName(), item);
                        }
                        if(mapList.size() != 0) break;
                    }
                }
            }
            else {
                Map<String, Documents> partOfMapList = getPartOfMapList(lastElementName, maxResults, i);
                for (Map.Entry<String, Documents> entry : partOfMapList.entrySet()){
                    mapList.put(entry.getKey(), entry.getValue());
                }
            }
        }
        Map<String, Documents> treeMapForSort = new TreeMap<>(mapList);
        List<Documents> finalList = new ArrayList<>();
        int count = 0;
        for (Map.Entry<String, Documents> entry : treeMapForSort.entrySet()){
            if(count < maxResults) finalList.add(entry.getValue());
            count++;
        }
        return finalList;
    }

    private Map<String, Documents> getPartOfMapList(String lastElementName, int maxResults, int nodesGroupNumber){
        Map<String, Documents> mapList = new HashMap<>();
        nodesList = getNodesList();
        String number = String.valueOf(nodesGroupNumber);
        List<String> nodes = nodesList.get(number);
        for (String node : nodes) {
            URI uri;
            try {
                uri = new URI("http://" + node + "/documentForNode/list/" + lastElementName + "/" + maxResults);
            } catch (URISyntaxException e) {
                logger.error("URISyntaxException", e);
                continue;
            }
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<List<Documents>> responseEntity1 = restTemplate.exchange(uri, HttpMethod.GET, httpEntity,
                    new ParameterizedTypeReference<List<Documents>>() {});
            List<Documents> list = responseEntity1.getBody();
            logger.info("documents was got for list from : {}", node);
            assert list != null;
            for (Documents aList : list) {
                mapList.put(aList.getName(), aList);
            }
            if(mapList.size() != 0) break;
            logger.info("");
        }
        return mapList;
    }

    private void create(String node, Documents document){
        URI uri = null;
        try {
            uri = new URI("http://" + node + "/documentForNode");
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException", e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<Documents> httpEntity = new HttpEntity<>(document, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Documents> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, Documents.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            logger.info("document was created in : {}", node);
        }
    }

    private void delete(String documentName, String collectionName, String node){
        URI uri = null;
        try {
            uri = new URI("http://" + node + "/documentForNode/" + documentName + "/" + collectionName);
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException", e);
        }
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity1 = new HttpEntity<>(null, headers1);
        RestTemplate restTemplate1 = new RestTemplate();
        ResponseEntity<String> responseEntity1 = restTemplate1.exchange(uri, HttpMethod.DELETE, httpEntity1, String.class);
        if (responseEntity1 != null && responseEntity1.getStatusCode() == HttpStatus.OK) {
            logger.info("Collection was delete from : {}", node);
        }
    }

    private void update(String name, String newName, String json, String collection_name, String node){
        URI uri = null;
        try {
            uri = new URI("http://" + node + "/documentForNode/" + name + "/" + newName + "/" + json + "/" + collection_name);
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException", e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, httpEntity, String.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            logger.info("");
            logger.info("document was updated in : {}", node);
        }
    }

    private boolean checkForCreate(List<String> nodes, Documents document){
        boolean check = true;
        for (String node : nodes) {
            Documents checkDocument = null;
            if ("selfNode".equals(node)){
                checkDocument = documentsService.getDocumentByName(document.getName(), document.getCollection_name());
                logger.info("document was got for checking");
            }
            else {
                URI uri2;
                try { uri2 = new URI("http://" + node + "/documentForNode/" + document.getName() + "/" + document.getCollection_name());
                } catch (URISyntaxException e) {
                    logger.error("URISyntaxException", e);
                    continue;
                }
                HttpHeaders headers2 = new HttpHeaders();
                headers2.add("Content-Type", "application/json");
                HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                RestTemplate restTemplate2 = new RestTemplate();
                ResponseEntity<Documents> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, Documents.class);
                if (responseEntity2 != null && responseEntity2.getStatusCode() == HttpStatus.OK) {
                    checkDocument = responseEntity2.getBody();
                    logger.info("document was got for checking from : {}", node);
                }
            }
            if(checkDocument == null) {
                check = false;
                break;
            }
        }
        return check;
    }

    private boolean check(List<String> nodes, String documentName, String collectionName){
        boolean check = true;
        for (String node : nodes) {
            Documents checkDocument = null;
            if ("selfNode".equals(node)){
                checkDocument = documentsService.getDocumentByName(documentName, collectionName);
                logger.info("document was got for checking");
            }
            else {
                URI uri2;
                try { uri2 = new URI("http://" + node + "/documentForNode/" + documentName + "/" + collectionName);
                } catch (URISyntaxException e) {
                    logger.error("URISyntaxException", e);
                    continue;
                }
                HttpHeaders headers2 = new HttpHeaders();
                headers2.add("Content-Type", "application/json");
                HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                RestTemplate restTemplate2 = new RestTemplate();
                ResponseEntity<Documents> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, Documents.class);
                if (responseEntity2 != null && responseEntity2.getStatusCode() == HttpStatus.OK) {
                    checkDocument = responseEntity2.getBody();
                    logger.info("document was got for checking from : {}", node);
                    logger.info("");
                }
            }
            if(checkDocument != null) {
                check = false;
                break;
            }
        }
        return check;
    }

    private void rollbackForCreate(List<String> nodes, Documents document){
        logger.info("create rollback");
        for (String node : nodes) {
            Documents checkDocument = null;
            if ("selfNode".equals(node)){
                checkDocument = documentsService.getDocumentByName(document.getName(), document.getCollection_name());
                logger.info("document was got for checking");
                if(checkDocument != null){
                    documentsService.deleteDocument(document.getName(), document.getCollection_name());
                    logger.info("document was delete");
                }
            }
            else {
                URI uri2;
                try { uri2 = new URI("http://" + node + "/documentForNode/" + document.getName() + "/" + document.getCollection_name());
                } catch (URISyntaxException e) {
                    logger.error("URISyntaxException", e);
                    continue;
                }
                HttpHeaders headers2 = new HttpHeaders();
                headers2.add("Content-Type", "application/json");
                HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                RestTemplate restTemplate2 = new RestTemplate();
                ResponseEntity<Documents> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, Documents.class);
                if (responseEntity2 != null && responseEntity2.getStatusCode() == HttpStatus.OK) {
                    checkDocument = responseEntity2.getBody();
                }
                if(checkDocument != null){
                    delete(document.getName(), document.getCollection_name(), node);
                }
            }
        }
    }

    private void rollbackForDelete(List<String> nodes, String documentName, String collectionName, Documents document){
        logger.info("Delete rollback");
        for (String node : nodes) {
            Documents checkDocument = null;
            if ("selfNode".equals(node)) {
                checkDocument = documentsService.getDocumentByName(documentName, collectionName);
                logger.info("document was got for checking");
                if(checkDocument == null){
                    documentsService.createDocument(document);
                    logger.info("document was created");
                }
            }
            else {
                URI uri2;
                try { uri2 = new URI("http://" + node + "/documentForNode/" + documentName + "/" + collectionName);
                } catch (URISyntaxException e) {
                    logger.error("URISyntaxException", e);
                    continue;
                }
                HttpHeaders headers2 = new HttpHeaders();
                headers2.add("Content-Type", "application/json");
                HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                RestTemplate restTemplate2 = new RestTemplate();
                ResponseEntity<Documents> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, Documents.class);
                if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                    checkDocument = responseEntity2.getBody();
                }
                if(checkDocument == null){
                    create(node, document);
                }
            }
        }
    }

    private void rollbackForUpdate(List<String> nodes, String name, String newName, String collection_name, Documents document){
        logger.info("Update rollback");
        for (String node : nodes) {
            Documents checkDocument = null;
            if ("selfNode".equals(node)) {
                checkDocument = documentsService.getDocumentByName(document.getName(), document.getCollection_name());
                logger.info("document was got for checking");
                if (checkDocument == null){
                    documentsService.deleteDocument(newName, collection_name);
                    documentsService.createDocument(document);
                    logger.info("document was updated");
                }
            }
            else {
                URI uri2;
                try {
                    uri2 = new URI("http://" + node + "/documentForNode/" + name + "/" + document.getCollection_name());
                } catch (URISyntaxException e) {
                    logger.error("URISyntaxException", e);
                    continue;
                }
                HttpHeaders headers1 = new HttpHeaders();
                headers1.add("Content-Type", "application/json");
                HttpEntity<String> httpEntity1 = new HttpEntity<>(null, headers1);
                RestTemplate restTemplate1 = new RestTemplate();
                ResponseEntity<Documents> responseEntity1 = restTemplate1.exchange(uri2, HttpMethod.GET, httpEntity1, Documents.class);
                checkDocument = responseEntity1.getBody();
                if (checkDocument == null) {
                    delete(newName, collection_name, node);
                    create(node, document);
                    logger.info("document was updated");
                }
            }
        }
    }
}
