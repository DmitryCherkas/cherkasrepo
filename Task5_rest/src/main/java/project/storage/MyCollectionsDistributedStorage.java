package project.storage;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import project.entities.MyCollections;
import project.entities.SmallMyCollections;
import project.interfaces.MyCollectionsService;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Repository
public class MyCollectionsDistributedStorage {

    @Autowired
    private MyCollectionsService myCollectionsService;

    private static Logger logger  = LoggerFactory.getLogger(MyCollectionsDistributedStorage.class);
    protected int selfNode;
    protected Map<Integer, List<String>> nodesList;

    public int getSelfNode(){
        for (Map.Entry<Integer, List<String>> entry : nodesList.entrySet()){
            int factor = Integer.parseInt(String.valueOf(entry.getKey()));
            List<String> list = entry.getValue();
            for (String item : list){ if ("selfNode".equals(item)){ selfNode = factor; } }
        }
        return selfNode;
    }

    public Map<Integer, List<String>> getNodesList(){
        JsonParser parser = new JsonParser();
        JsonElement jsonObj = null;
        try {
            jsonObj = parser.parse(new FileReader("src/main/resources/nodeList.json"));
            nodesList  = new HashMap<>(new Gson().fromJson(jsonObj.toString(),Map.class));
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException", e);
        }
        return nodesList;
    }

    public MyCollections getMyCollectionByName(String collectionName){
        MyCollections collection = myCollectionsService.getMyCollectionByName(collectionName);
        if (collection != null) { return collection; }
        logger.info("Collection doesn't exist");
        return null;
    }

    public void addMyCollection(MyCollections collection){
        nodesList = getNodesList();
        selfNode = getSelfNode();
        for(int i = 0; i < nodesList.size(); i++) {
            String number = null;
            if(i == selfNode) {
                number = String.valueOf(selfNode);
                List<String> nodes = nodesList.get(number);
                for (String node : nodes) {
                    if ("selfNode".equals(node)) {
                        myCollectionsService.createMyCollection(collection);
                        logger.info("Collection was created");
                    } else { create(node, collection); }
                }
            }
            else{
                number = String.valueOf(i);
                List<String> nodes = nodesList.get(number);
                for (String node : nodes) { create(node, collection); }
            }
        }
        if(!checkForCreate(collection.getName(), collection)){
            rollbackForCreate(collection);
        }
    }

    public void deleteMyCollection(String  collectionName){
        MyCollections collection = myCollectionsService.getMyCollectionByName(collectionName);
        nodesList = getNodesList();
        String number = String.valueOf(getSelfNode());
        if(collection == null) {
            logger.info("Collection doesn't exist");
        }
        else {
            for (int i = 0; i < nodesList.size(); i++) {
                if(i != selfNode) {
                    String num = String.valueOf(i);
                    List<String> nodes = nodesList.get(num);
                    for (String node : nodes) { delete(collectionName, node); }
                }
                else{
                    List<String> nodes = nodesList.get(number);
                    for (String node : nodes) {
                        if ("selfNode".equals(node)) {
                            myCollectionsService.deleteMyCollection(collectionName);
                            logger.info("Collection was delete");
                        }
                        else { delete(collectionName, node); }
                    }
                }
            }
            if(!check(collectionName, collection)){
                rollbackForDelete(collectionName, collection);
            }
        }
    }

    public void updateCollection(String name, String newName, int limit, String algorithm) {
        MyCollections collection = myCollectionsService.getMyCollectionByName(name);
        nodesList = getNodesList();
        String number = String.valueOf(getSelfNode());
        if(collection == null) {
            logger.info("Collection doesn't exist");
        }
        else {
            for (int i = 0; i < nodesList.size(); i++) {
                if(i != selfNode) {
                    String num = String.valueOf(i);
                    List<String> nodes = nodesList.get(num);
                    MyCollections check = null;
                    for (String node : nodes) { update(name, newName, limit, algorithm, node); }
                }
                else {
                    List<String> nodes = nodesList.get(number);
                    for (String node : nodes) {
                        if ("selfNode".equals(node)) {
                            myCollectionsService.updateMyCollection(name, newName, limit, algorithm);
                            logger.info("Collection was updated");
                        }
                        else { update(name, newName, limit, algorithm, node); }
                    }
                }
            }
            if(!check(name, collection)){
                rollbackForUpdate(name, newName, limit, algorithm);
            }
        }
    }

    public List<SmallMyCollections> list(String lastElementName, int maxResults){
        List<SmallMyCollections> list = myCollectionsService.list(lastElementName, maxResults);
        logger.info("Collections was got for list");
        return list;
    }

    private void create(String node, MyCollections collection){
        URI uri = null;
        try {
            uri = new URI("http://" + node + "/mycollectionForNode");
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException", e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<MyCollections> httpEntity = new HttpEntity<>(collection, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<MyCollections> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, MyCollections.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            logger.info("Collection was created in : {}", node);
        }
    }

    private void delete(String  collectionName,String node){
        URI uri = null;
        try {
            uri = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException", e);
        }
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity1 = new HttpEntity<>(null, headers1);
        RestTemplate restTemplate1 = new RestTemplate();
        ResponseEntity<String> responseEntity1 = restTemplate1.exchange(uri, HttpMethod.DELETE, httpEntity1, String.class);
        if (responseEntity1 != null && responseEntity1.getStatusCode() == HttpStatus.OK) {
            logger.info("Collection was delete from : {}", node);
        }
    }

    private void update(String name, String newName, int limit, String algorithm, String node){
        URI uri = null;
        try {
            uri = new URI("http://" + node + "/mycollectionForNode/" + name + "/" + newName + "/" + limit + "/" + algorithm);
        } catch (URISyntaxException e) {
            logger.error("URISyntaxException", e);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, httpEntity, String.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            logger.info("Collection was updated in : {}", node);
        }
    }

    private void rollbackForCreate(MyCollections collection){
        logger.info("create rollback");
        nodesList = getNodesList();
        selfNode = getSelfNode();
        MyCollections checkCollection = null;
        for(int i = 0; i < nodesList.size(); i++) {
            if (i == selfNode) {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    if ("selfNode".equals(node)) {
                        checkCollection = myCollectionsService.getMyCollectionByName(collection.getName());
                        logger.info("Collection was got for checking");
                        if (checkCollection == null) {
                            myCollectionsService.deleteMyCollection(collection.getName());
                            logger.info("Collection was delete");
                        }
                    } else {
                        URI uri2 = null;
                        try {
                            uri2 = new URI("http://" + node + "/mycollectionForNode/" + collection.getName());
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                        }
                        HttpHeaders headers2 = new HttpHeaders();
                        headers2.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                        RestTemplate restTemplate2 = new RestTemplate();
                        ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                        if (responseEntity2 != null && responseEntity2.getStatusCode() == HttpStatus.OK) {
                            logger.info("Collection was got from : {} for checking", node);
                            checkCollection = responseEntity2.getBody();
                        }
                        if (checkCollection == null) {
                            delete(collection.getName(), node);
                            logger.info("");
                        }
                    }
                }
            }
            else {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    URI uri2 = null;
                    try {
                        uri2 = new URI("http://" + node + "/mycollectionForNode/" + collection.getName());
                    } catch (URISyntaxException e) {
                        logger.error("URISyntaxException", e);
                    }
                    HttpHeaders headers2 = new HttpHeaders();
                    headers2.add("Content-Type", "application/json");
                    HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                    RestTemplate restTemplate2 = new RestTemplate();
                    ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                    if (responseEntity2 != null && responseEntity2.getStatusCode() == HttpStatus.OK) {
                        logger.info("Collection was got from : {} for checking", node);
                        checkCollection = responseEntity2.getBody();
                    }
                    if (checkCollection == null) {
                        delete(collection.getName(), node);
                    }
                }
            }
        }
    }

    private boolean check(String  collectionName, MyCollections collection){
        nodesList = getNodesList();
        selfNode = getSelfNode();
        boolean check = true;
        for(int i = 0; i < nodesList.size(); i++) {
            MyCollections checkCollection = null;
            if (i == selfNode) {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    if ("selfNode".equals(node)) {
                        checkCollection = myCollectionsService.getMyCollectionByName(collection.getName());
                        logger.info("Collection was got for checking");
                    } else {
                        URI uri2;
                        try {
                            uri2 = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers2 = new HttpHeaders();
                        headers2.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                        RestTemplate restTemplate2 = new RestTemplate();
                        ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                        if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                            logger.info("Collection was got from : {} for checking", node);
                            checkCollection = responseEntity2.getBody();
                        }
                    }
                }
            }
            else {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    URI uri2;
                    try {
                        uri2 = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
                    } catch (URISyntaxException e) {
                        logger.error("URISyntaxException", e);
                        continue;
                    }
                    HttpHeaders headers2 = new HttpHeaders();
                    headers2.add("Content-Type", "application/json");
                    HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                    RestTemplate restTemplate2 = new RestTemplate();
                    ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                    if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                        logger.info("Collection was got from : {} for checking", node);
                        logger.info("");
                        checkCollection = responseEntity2.getBody();
                    }
                }
            }
            if(checkCollection != null) {
                check = false;
                break;
            }
        }
        return check;
    }

    private boolean checkForCreate(String  collectionName, MyCollections collection){
        nodesList = getNodesList();
        selfNode = getSelfNode();
        boolean check = true;
        for(int i = 0; i < nodesList.size(); i++) {
            MyCollections checkCollection = null;
            if (i == selfNode) {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    if ("selfNode".equals(node)) {
                        checkCollection = myCollectionsService.getMyCollectionByName(collection.getName());
                        logger.info("Collection was got for checking");
                    } else {
                        URI uri2;
                        try {
                            uri2 = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers2 = new HttpHeaders();
                        headers2.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                        RestTemplate restTemplate2 = new RestTemplate();
                        ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                        if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                            logger.info("");
                            logger.info("Collection was got from : {} for checking", node);
                            checkCollection = responseEntity2.getBody();
                        }
                    }
                }
            }
            else {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    URI uri2;
                    try {
                        uri2 = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
                    } catch (URISyntaxException e) {
                        logger.error("URISyntaxException", e);
                        continue;
                    }
                    HttpHeaders headers2 = new HttpHeaders();
                    headers2.add("Content-Type", "application/json");
                    HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                    RestTemplate restTemplate2 = new RestTemplate();
                    ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                    if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                        logger.info("Collection was got from : {} for checking", node);
                        checkCollection = responseEntity2.getBody();
                        logger.info("");
                    }
                }
            }
            if(checkCollection == null) {
                check = false;
                break;
            }
        }
        return check;
    }

    private void rollbackForDelete(String  collectionName, MyCollections collection){
        logger.info("delete rollback");
        nodesList = getNodesList();
        selfNode = getSelfNode();
        MyCollections checkCollection = null;
        for(int i = 0; i < nodesList.size(); i++) {
            if (i == selfNode) {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    if ("selfNode".equals(node)) {
                        checkCollection = myCollectionsService.getMyCollectionByName(collection.getName());
                        logger.info("Collection was got for checking");
                        if (checkCollection == null) {
                            myCollectionsService.createMyCollection(collection);
                            logger.info("Collection was created again");
                        }
                    } else {
                        URI uri2;
                        try {
                            uri2 = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers2 = new HttpHeaders();
                        headers2.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                        RestTemplate restTemplate2 = new RestTemplate();
                        ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                        if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                            logger.info("Collection was got from : {} for checking", node);
                            checkCollection = responseEntity2.getBody();
                        }
                        if (checkCollection == null) {
                            create(node, collection);
                            logger.info("Collection was created again");
                        }
                    }
                }
            }
            else {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    URI uri2;
                    try {
                        uri2 = new URI("http://" + node + "/mycollectionForNode/" + collectionName);
                    } catch (URISyntaxException e) {
                        logger.error("URISyntaxException", e);
                        continue;
                    }
                    HttpHeaders headers2 = new HttpHeaders();
                    headers2.add("Content-Type", "application/json");
                    HttpEntity<String> httpEntity2 = new HttpEntity<>(null, headers2);
                    RestTemplate restTemplate2 = new RestTemplate();
                    ResponseEntity<MyCollections> responseEntity2 = restTemplate2.exchange(uri2, HttpMethod.GET, httpEntity2, MyCollections.class);
                    if (responseEntity2.getStatusCode() == HttpStatus.OK) {
                        logger.info("Collection was got from : {} for checking", node);
                        checkCollection = responseEntity2.getBody();
                    }
                    if (checkCollection == null) {
                        create(node, collection);
                        logger.info("Collection was created again");
                        logger.info("");
                    }
                }
            }
        }
    }

    private void rollbackForUpdate(String name, String newName, int limit, String algorithm){
        logger.info("Update rollback");
        nodesList = getNodesList();
        selfNode = getSelfNode();
        MyCollections checkCollection = null;
        for(int i = 0; i < nodesList.size(); i++) {
            if (i == selfNode) {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    if ("selfNode".equals(node)) {
                        checkCollection = myCollectionsService.getMyCollectionByName(name);
                        logger.info("Collection was got for checking");
                        if (checkCollection != null) {
                            myCollectionsService.updateMyCollection(name, newName, limit, algorithm);
                            logger.info("Collection was updated");
                        }
                    } else {
                        URI uri2;
                        try {
                            uri2 = new URI("http://" + node + "/mycollectionForNode/" + name);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers1 = new HttpHeaders();
                        headers1.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity1 = new HttpEntity<>(null, headers1);
                        RestTemplate restTemplate1 = new RestTemplate();
                        ResponseEntity<MyCollections> responseEntity1 = restTemplate1.exchange(uri2, HttpMethod.GET, httpEntity1, MyCollections.class);
                        checkCollection = responseEntity1.getBody();
                        logger.info("Collection was got from : {} for checking", node);
                        if (checkCollection != null) {
                            URI uri;
                            try {
                                uri = new URI("http://" + node + "/mycollectionForNode/" + name);
                            } catch (URISyntaxException e) {
                                logger.error("URISyntaxException", e);
                                continue;
                            }
                            HttpHeaders headers = new HttpHeaders();
                            headers.add("Content-Type", "application/json");
                            HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
                            RestTemplate restTemplate = new RestTemplate();
                            ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, httpEntity, String.class);
                            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                                logger.info("Collection was updated in : {}", node);
                            }
                        }
                    }
                }
            }
            else {
                String num = String.valueOf(i);
                List<String> nodes = nodesList.get(num);
                for (String node : nodes) {
                    URI uri2;
                    try {
                        uri2 = new URI("http://" + node + "/mycollectionForNode/" + name);
                    } catch (URISyntaxException e) {
                        logger.error("URISyntaxException", e);
                        continue;
                    }
                    HttpHeaders headers1 = new HttpHeaders();
                    headers1.add("Content-Type", "application/json");
                    HttpEntity<String> httpEntity1 = new HttpEntity<>(null, headers1);
                    RestTemplate restTemplate1 = new RestTemplate();
                    ResponseEntity<MyCollections> responseEntity1 = restTemplate1.exchange(uri2, HttpMethod.GET, httpEntity1, MyCollections.class);
                    checkCollection = responseEntity1.getBody();
                    logger.info("Collection was got from : {} for checking", node);
                    if (checkCollection != null) {
                        URI uri;
                        try {
                            uri = new URI("http://" + node + "/mycollectionForNode/" + name);
                        } catch (URISyntaxException e) {
                            logger.error("URISyntaxException", e);
                            continue;
                        }
                        HttpHeaders headers = new HttpHeaders();
                        headers.add("Content-Type", "application/json");
                        HttpEntity<String> httpEntity = new HttpEntity<>(null, headers);
                        RestTemplate restTemplate = new RestTemplate();
                        ResponseEntity<String> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, httpEntity, String.class);
                        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                            logger.info("Collection was updated in : {}", node);
                            logger.info("");
                        }
                    }
                }
            }
        }
    }
}
