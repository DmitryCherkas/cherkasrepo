import org.junit.Test;
import project.caches.LFUCacheImpl;
import project.caches.LRUCacheImpl;
import project.servicesImpl.DocumentsServiceImpl;

import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ProjectTests {

    private LFUCacheImpl cache;

    private LRUCacheImpl cache2;

    @Test
    public void createLFUCache(){ this.cache = new LFUCacheImpl(2); }

    @Test
    public void youWantToGetNode(){
        LFUCacheImpl cache = new LFUCacheImpl(2);
        cache.put(1, 1);
        assertEquals(cache.get(1), 1);
        assertNull(cache.get(2));
    }

    @Test
    public void cashHaventNodeWithThisKey(){
        LFUCacheImpl cache = new LFUCacheImpl(2);
        cache.put(1, 1);
        assertNull(cache.get(6));
    }

    @Test
    public void testOnContains(){
        LFUCacheImpl cache = new LFUCacheImpl(2);
        cache.put(1, 1);
        assertTrue(cache.contains(1));
        assertFalse(cache.contains(11));
    }

    @Test
    public void testOnSize() {
        LFUCacheImpl cache = new LFUCacheImpl(2);
        cache.put(1, 1);
        cache.put(2, 6);
        cache.put(3, 5);
        int size = cache.size();
        assertEquals(size, 2);
    }

    @Test
    public void testRemoveNodeWithMinFrequency(){
        LFUCacheImpl cache = new LFUCacheImpl(2);
        cache.put(1, 1);
        cache.put(2, 6);
        cache.put(3, 5);
        assertNull(cache.get(2));
        assertEquals(cache.get(1), 1);
        assertEquals(cache.get(3), 5);
        cache.put(4, 2);
        assertEquals(cache.get(1), 1);
        assertNull(cache.get(3));
        assertEquals(cache.get(4), 2);
    }

    @Test
    public void testOnFrequencyChanging(){
        LFUCacheImpl cache = new LFUCacheImpl(2);
        cache.put(1, 1);
        cache.put(2, 6);
        cache.put(1, 1);
        cache.put(3, 5);
        assertNull(cache.get(2));
        assertEquals(cache.get(1), 1);
        assertEquals(cache.get(3), 5);
    }

    @Test
    public void testOnPutIfAbsent(){
        LFUCacheImpl cache = new LFUCacheImpl(2);
        assertEquals(cache.putIfAbsent(1, 1), 1);
    }

    @Test
    public void createLRUCache(){
        this.cache2 = new LRUCacheImpl(2);
    }

    @Test
    public void youWantToGetNodeLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put(1, 1);
        assertEquals(cache.get(1), 1);
        assertNull(cache.get(2));
    }

    @Test
    public void cashHaventNodeWithThisKeyLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put(1, 1);
        assertNull(cache.get(6));
    }

    @Test
    public void testOnSizeLRU() {
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put(1, 1);
        cache.put(2, 6);
        cache.put(3, 5);
        int size = cache.size();
        assertEquals(size, 2);
    }

    @Test
    public void removeIsLeastRecentlyUsedLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put(1, 1);
        cache.put(2, 6);
        cache.put(3, 5);
        assertNull(cache.get(1));
        assertEquals(cache.get(2), 6);
        assertEquals(cache.get(3), 5);
        cache.put(4, 2);
        assertNull(cache.get(2));
        assertEquals(cache.get(3), 5);
        assertEquals(cache.get(4), 2);
    }

    @Test
    public void testOnMoveThroughTheListLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put(1, 1);
        cache.put(2, 6);
        cache.get(1);
        cache.put(3, 5);
        assertEquals(cache.get(1), 1);
        assertNull(cache.get(2));
        assertEquals(cache.get(3), 5);
    }

    @Test
    public void testOnContainsLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put("dfsrgw", "ffffffffffff");
        assertTrue(cache.contains("dfsrgw"));
        assertFalse(cache.contains(2));
    }

    @Test
    public void testOnPutIfAbsentLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        assertEquals(cache.putIfAbsent(1, 1), 1);

    }

    @Test
    public void TestOnPutIfContainsFindKeyLRU(){
        LRUCacheImpl cache = new LRUCacheImpl(2);
        cache.put("dfsrgw", "ffffffffffff");
        cache.put(1, "fffffffffffgthnf");
        cache.put("dfsrgw", "ffffffffffff");
        cache.put(2, 1);
        assertEquals(cache.get("dfsrgw"), "ffffffffffff");
        assertNull(cache.get(1));
    }

    @Test
    public void getCacheLfuTest(){
        DocumentsServiceImpl documentsService = new DocumentsServiceImpl();
        assertNull(documentsService.getCache("abracadabra", 3));
    }
}
